package be.hdp.sanity.example;

import be.hdp.sanity.example.page.GoogleFrontPage;
import be.hdp.sanity.example.util.Browser;
import be.hdp.sanity.example.util.WebdriverFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * Test performing a search on google.
 */
public class GoogleSearchTest {

    private static final Logger LOG = LoggerFactory.getLogger(GoogleSearchTest.class);

    private static WebDriver driver;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        driver = WebdriverFactory.getInstance(Browser.CHROME).getDriver(new HashMap<String, String>());
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    WebdriverFactory.getInstance().quitDriver();
                } catch (Exception e) {
                    LOG.error("Exception while shutting down browser", e);
                }
            }
        });
    }

    @Before
    public void setUp() throws Exception {
        driver.get("http://www.google.be");
    }

    @Test
    public void testGoogleSearch() throws Exception {
        GoogleFrontPage frontPage = new GoogleFrontPage();
        frontPage = frontPage.typeInSearchInput("hdp");
        frontPage.clickSearchButton();
    }

}
