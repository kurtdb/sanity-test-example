package be.hdp.sanity.example.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ThreadGuard;

import java.util.Map;

/**
 * Wrapper around the chrome driver.
 */
public class ChromeDriverWrapper implements WebDriverWrapper {
    @Override
    public WebDriver create(Map<String, String> settings) {
        System.setProperty("webdriver.chrome.driver", "c:/drivers/chromedriver.exe");
        return ThreadGuard.protect(new ChromeDriver());
    }

    @Override
    public void quit(WebDriver driver) throws Exception {
        if (driver != null) {
            driver.quit();
        }
    }
}
