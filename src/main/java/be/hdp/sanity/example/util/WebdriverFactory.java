package be.hdp.sanity.example.util;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Object wrapping the creation and distribution of the webdriver drivers.
 */
public class WebdriverFactory {

    private static final Logger LOG = LoggerFactory.getLogger(WebdriverFactory.class);

    private static WebdriverFactory instance;

    private static WebDriverWrapper wrapper;

    private static ThreadLocal<WebDriver> browserThreadLocal = new ThreadLocal<>();

    private WebdriverFactory() {
    }

    public static WebdriverFactory getInstance(Browser browser) {
        LOG.debug("Creating new webdriverfactory for browser: " + browser.getBrowserName());
        if (instance == null) {
            instance = new WebdriverFactory();
            wrapper = browser.getWrapper();
        }
        return instance;
    }

    public static WebdriverFactory getInstance() {
        LOG.debug("Getting instance of webdriverfactory");
        return instance;
    }

    public WebDriver getDriver(Map<String, String> settings) {
        LOG.info("Getting driver with settings.");
        if (browserThreadLocal.get() == null) {
            LOG.info("Creating new browser with settings.");
            browserThreadLocal.set(wrapper.create(settings));
        }
        return browserThreadLocal.get();
    }

    public WebDriver getDriver() {
        LOG.info("Getting browser from the threadlocal.");
        return (WebDriver) browserThreadLocal.get();
    }

    public void quitDriver() throws Exception {
        wrapper.quit(browserThreadLocal.get());
        browserThreadLocal.remove();
        instance = null;
    }
}
