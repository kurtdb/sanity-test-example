package be.hdp.sanity.example.util;

/**
 * Utility class defining all the names of the browsers.
 */
public enum Browser {
    CHROME("chrome", new ChromeDriverWrapper());

    private WebDriverWrapper wrapper;

    private String browserName;

    private Browser(String browsername, WebDriverWrapper wrapper) {
        this.wrapper = wrapper;
        this.browserName = browsername;
    }

    public WebDriverWrapper getWrapper() {
        return this.wrapper;
    }

    public String getBrowserName() {
        return browserName;
    }

    public static Browser forName(String browserName) {
        Browser[] browsers = values();
        for (Browser browser : browsers) {
            if (browser.getBrowserName().equalsIgnoreCase(browserName)) {
                return browser;
            }
        }
        return null;
    }
}
