package be.hdp.sanity.example.util;

import org.openqa.selenium.WebDriver;

import java.util.Map;

/**
 * Interface defining the default lifecycle events for a webdriver wrapper.
 */
public interface WebDriverWrapper {

    WebDriver create(Map<String, String> settings);

    void quit(WebDriver driver) throws Exception;
}
