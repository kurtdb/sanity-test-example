package be.hdp.sanity.example.page;

import be.hdp.sanity.example.util.WebdriverFactory;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class implementing all boilerplate methods that are to be performed on a page.
 */
public abstract class Page {

    private static final Logger LOG = LoggerFactory.getLogger(Page.class);

    private WebDriver driver;

    public Page() {
        this(WebdriverFactory.getInstance().getDriver());
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
    }

    public Page(WebDriver driver) {
        this.driver = driver;
        assertCorrectPage();
    }

    private void assertCorrectPage() {
        LOG.info("Verifying we are on the correct page.");
        Assert.assertTrue("The expected string was not on the page, so we are not on the correct page", driver.getPageSource().contains(getExpectedStringOnPage()));
    }

    public abstract String getExpectedStringOnPage();

    public void waitForJavascript() {
        LOG.info("Waiting for the page to be loaded.");
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(javascriptDoneLoading());
    }

    private ExpectedCondition<Boolean> javascriptDoneLoading() {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                return (executor.executeScript("return document.readyState").equals("complete") || executor.executeScript("return document.readyState").equals("interactive"));
            }
        };
    }
}
