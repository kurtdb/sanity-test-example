package be.hdp.sanity.example.page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Object representation of the google front page.
 */
public class GoogleFrontPage extends Page {

    private static final Logger LOG = LoggerFactory.getLogger(GoogleFrontPage.class);

    @FindBy(id = "gbqfq")
    private WebElement searchInput;

    @FindBy(id = "gbqfb")
    private WebElement searchButton;

    @Override
    public String getExpectedStringOnPage() {
        return "Google zoeken";
    }

    public GoogleFrontPage typeInSearchInput(String input) {
        LOG.info("Typing the following text in the input: " + input);
        searchInput.clear();
        searchInput.sendKeys(input);
        return new GoogleFrontPage();
    }

    public GoogleResultPage clickSearchButton() {
        LOG.info("Clicking the search button.");
        searchButton.click();
        waitForJavascript();
        return new GoogleResultPage();
    }


}
