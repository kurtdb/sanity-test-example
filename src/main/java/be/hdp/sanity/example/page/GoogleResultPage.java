package be.hdp.sanity.example.page;

/**
 * Object representation of the google result page.
 */
public class GoogleResultPage extends Page {
    @Override
    public String getExpectedStringOnPage() {
        return "Ongeveer ";
    }
}
